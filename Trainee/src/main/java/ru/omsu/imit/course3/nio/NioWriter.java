package ru.omsu.imit.course3.nio;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class NioWriter {
    private static final int FILE_SIZE = 100;
    public static void mappedByteBufferWrite(String filePath) throws IOException {
        int[] arr = new int[FILE_SIZE];
        for(int i = 0; i < FILE_SIZE; i++){arr[i]=i;}
        try(FileChannel fc = FileChannel.open(Paths.get(filePath), new StandardOpenOption[]{StandardOpenOption.CREATE,
                StandardOpenOption.READ, StandardOpenOption.WRITE}))
        {
            MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_WRITE, -0, arr.length);

            for(int b: arr){
                mbb.put((byte)b);
            }

        }
    }

    public static void main(String[] args) throws IOException {
        mappedByteBufferWrite("Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\nio\\files\\33rd.txt");

    }
}
