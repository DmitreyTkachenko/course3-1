package ru.omsu.imit.course3.nio;

import ru.omsu.imit.course3.Trainee.Trainee;
import ru.omsu.imit.course3.Trainee.TraineeException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.MappedByteBuffer;
import java.util.Arrays;

public class NioReader {
    public static Trainee byteBufferReaderFromFile(String filePath) throws TraineeException, IOException {
             try(FileChannel fc = FileChannel.open(Paths.get(filePath), StandardOpenOption.READ)) {
                 ByteBuffer bb = ByteBuffer.allocate((int) fc.size());   //allocate-Выделяет новый байтовый буфер.
                 fc.read(bb);
                 String[] data = new String(bb.array(), StandardCharsets.UTF_8).split(" ");
                 return new Trainee(data[0], data[1], Integer.parseInt(data[2]));
             }
    }

    public static Trainee mappedByteBufferReaderFromFile (String filePath) throws TraineeException, IOException {
       try(FileChannel fc = new RandomAccessFile(filePath, "r").getChannel()) {
           MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
           byte[] bytes = new byte[(int) fc.size()];
           mbb.get(bytes);
           String[] data = new String(bytes, StandardCharsets.UTF_8).split(" ");
           return new Trainee(data[0], data[1], Integer.parseInt(data[2]));
       }
    }

    public static String read() throws  IOException {
        FileInputStream fin=new FileInputStream("Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\nio\\files\\3rd.txt");
        byte[] buffer = new byte[fin.available()];
        fin.read(buffer, 0, buffer.length);
        StringBuilder str = new StringBuilder();
        for (byte bb: buffer) {
            str.append(bb);
        }
        return str.toString();
    }

    public static void main(String[] args) throws IOException, TraineeException {
       Trainee test = byteBufferReaderFromFile("Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\Trainee\\files\\trainee.txt");
        System.out.println(test.getName()+test.getSurname()+test.getScore());
        Trainee test2 = mappedByteBufferReaderFromFile("Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\Trainee\\files\\trainee.txt");
        System.out.println(test2.getName()+test2.getSurname()+test2.getScore());
        System.out.println(read());

    }
}
