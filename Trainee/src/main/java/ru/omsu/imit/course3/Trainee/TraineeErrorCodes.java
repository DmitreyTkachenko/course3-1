package ru.omsu.imit.course3.Trainee;


public enum TraineeErrorCodes {
    NAME_IS_EMPTY("can not be null or an empty string"),
    SURNAME_IS_EMPTY("can not be null or an empty string"),
    NOT_VALID_SCORE("allowable values from 1 to 5.");
    private final String errorCodes;

    TraineeErrorCodes(String errorCodes) {
        this.errorCodes = errorCodes;
    }


    public String getErrorCodes() {
        return errorCodes;
    }
}
