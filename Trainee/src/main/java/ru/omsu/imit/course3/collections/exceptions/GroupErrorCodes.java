package ru.omsu.imit.course3.collections.exceptions;


public enum GroupErrorCodes {
    NAME_IS_EMPTY("can not be null or an empty string"),
    TRAINEE_IS_NULL("can not be null or an empty string");
    private final String errorCodes;

    GroupErrorCodes(String errorCodes) {
        this.errorCodes = errorCodes;
    }


    public String getErrorCodes() {
        return errorCodes;
    }
}