package ru.omsu.imit.course3.collections.exceptions;

public class GroupExceptions extends Exception {
    public GroupExceptions(){
    }

    public GroupExceptions(String errorCodes) {
        super(errorCodes);
    }

}
