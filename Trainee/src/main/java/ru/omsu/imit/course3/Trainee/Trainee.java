package ru.omsu.imit.course3.Trainee;
/*DmTk*/
public class Trainee {
    private String name;//  * имя - текстовая строка, не может быть null или пустой строкой.
    private String surname;//  * фамилия -текстовая строка, не может быть null или пустой строкой.
    private int score;//   * оценка - целое, допустимые значения от 1 до 5.

    public Trainee(String name, String surname, int score) throws TraineeException {
        this.name = name;
        if(name == null) throw new TraineeException(TraineeErrorCodes.NAME_IS_EMPTY.getErrorCodes());
        if(name.trim().length() == 0) throw new TraineeException(TraineeErrorCodes.NAME_IS_EMPTY.getErrorCodes());
        this.surname = surname;
        if(surname == null) throw new TraineeException(TraineeErrorCodes.SURNAME_IS_EMPTY.getErrorCodes());
        if(surname.trim().length() == 0) throw new TraineeException(TraineeErrorCodes.SURNAME_IS_EMPTY.getErrorCodes());
        this.score = score;
        if(1 > score || 5 < score )throw new TraineeException(TraineeErrorCodes.NOT_VALID_SCORE.getErrorCodes());
    }

    public String getName()  {
        return name;
    }

    public void setName(String name)throws TraineeException {
        if(name == null) throw new TraineeException(TraineeErrorCodes.NAME_IS_EMPTY.getErrorCodes());
        if(name.trim().length() == 0) throw new TraineeException(TraineeErrorCodes.NAME_IS_EMPTY.getErrorCodes());
        this.name = name;
    }

    public String getSurname()throws TraineeException {
        return surname;
    }

    public void setSurname(String surname)throws TraineeException {
        if(surname == null) throw new TraineeException(TraineeErrorCodes.SURNAME_IS_EMPTY.getErrorCodes());
        if(surname.trim().length() == 0) throw new TraineeException(TraineeErrorCodes.SURNAME_IS_EMPTY.getErrorCodes());
        this.surname = surname;
    }
//
    public int getScore() {
        return score;
    }

    public void setScore(int score)throws TraineeException {
        if(1 > score || 5 < score )throw new TraineeException(TraineeErrorCodes.NOT_VALID_SCORE.getErrorCodes());
        this.score = score;
    }

    public int compareTo(Trainee o) {
        return name.compareTo(o.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trainee trainee = (Trainee) o;

        if (score != trainee.score) return false;
        if (!name.equals(trainee.name)) return false;
        return surname.equals(trainee.surname);
    }


    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + score;
        return result;
    }



    public static void main(String[] args) throws TraineeException {
        Trainee first = new Trainee("A","B", 5);
        System.out.println(first.getName() +" "+ first.getSurname() +" "+ first.getScore());
        first.setName("AAA"); first.setSurname("BBB");first.setScore(2);
        System.out.println(first.getName() +" "+ first.getSurname() +" "+ first.getScore());
    }


}
