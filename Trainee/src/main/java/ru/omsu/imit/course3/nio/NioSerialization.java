package ru.omsu.imit.course3.nio;

import ru.omsu.imit.course3.Trainee.TestMethods;
import ru.omsu.imit.course3.Trainee.Trainee;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class NioSerialization {

    public static ByteBuffer serializeToByteBuffer(Trainee trainee) throws IOException {
        String json = TestMethods.serialize(trainee);
        ByteBuffer bb = ByteBuffer.allocate(json.length());
        bb.put(json.getBytes());
        return bb;
    }

    public static Trainee deserializeFromByteBuffer(ByteBuffer bb) throws IOException, ClassNotFoundException {
        return TestMethods.deserialize(new String(bb.array(), StandardCharsets.UTF_8));
    }
}
