package ru.omsu.imit.course3.Trainee;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;




import java.io.File;
import java.io.IOException;

public class CheckFile {
    public static void main(String[] args) throws IOException {
        File file = new File("test\\file.txt");

        System.out.println(file.getName()+"      ←file Name");
        System.out.println(file.getAbsolutePath());
        if(!file.exists())
            System.out.println(file.createNewFile());
        else
            System.out.println(true+"      ←old file");
        file.renameTo(new File("test\\fileNewName"));
        System.out.println(file.isFile()+"      ←file.isFile()");
        file.delete();


        File dir = new File("Trainee\\test");

        if(!dir.exists())
            dir.mkdir();
        System.out.println(dir.isDirectory()+"      ←dir.isDirectory");
        System.out.println(dir.getName()+"      ←dir.getName()");
        System.out.println(dir.getAbsolutePath()+"      ←folder");

        if(dir.isDirectory()){
            for(File item : dir.listFiles()){
                if(item.isDirectory()){
                    System.out.println(item.getName() + " \t ");
                }
                else{
                    System.out.println(item.getName() + "\t   ←file");
                }
            }
        }

        dir.renameTo(new File("NewTest"));
        dir.delete();

        }
}















/*public class CheckFile {
    public static void main(String[] args) throws IOException{
        File file = new File("C:/Проекты студентов и преподавателей/3 курс/DimTk/course3-1/Trainee/src" +
                "/main/java/ru/omsu/imit/course3/Trainee/text.txt");
        File file1 = new File("C:/Проекты студентов и преподавателей/3 курс/DimTk/course3-1/Trainee/src" +
                "/main/java/ru/omsu/imit/course3/Trainee/texxxt.txt");
        System.out.println(file.getName());// имя файла

        if(!file.exists()) {
            System.out.println(file.createNewFile());
        }

        if(file1.exists() && file.exists()) {
            file1.deleteOnExit();
            file.deleteOnExit();
        }

            if(file.renameTo(file1)){

                System.out.println("File renamed");
                System.out.println(file.getAbsolutePath());


            }else{
                System.out.println("Sorry! the file can't be renamed");
            }
            System.out.println("ok");


        // System.out.println(file.getName());// имя файла после переименования

    }
}
*/
