package ru.omsu.imit.course3.Trainee;


public class TraineeException extends Exception {
    public TraineeException() {
    }

    public TraineeException(String errorCodes) {
        super(errorCodes);
        }

    public TraineeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TraineeException(Throwable cause) {
        super(cause);
    }

    public TraineeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
