package ru.omsu.imit.course3.collections;

import ru.omsu.imit.course3.Trainee.Trainee;
import ru.omsu.imit.course3.collections.exceptions.GroupExceptions;

import java.util.Arrays;
import java.util.Comparator;

public class GroupMethods {
    public static Group sortByMarks(Group group) throws GroupExceptions {
        Arrays.sort(group.getTrainees(), Comparator.comparingInt(Trainee::getScore));
        return new Group(group.getName(), group.getTrainees());
    }

    public static Group sortByNames(Group group) throws GroupExceptions {
        Arrays.sort(group.getTrainees(), Trainee::compareTo);
        return new Group(group.getName(), group.getTrainees());
    }

    public static Trainee findTrainee(Group group, String name) throws GroupExceptions {
        for(Trainee trainee: group.getTrainees())
            if(trainee.getName().equals(name))
                return trainee;

        return null;
    }
}
