package ru.omsu.imit.course3.collections;

import ru.omsu.imit.course3.collections.exceptions.InstituteErrorCodes;
import ru.omsu.imit.course3.collections.exceptions.InstituteExceptions;

import java.util.Objects;

public class Institute {
    String name;
    String city;

    public Institute(String name, String city) throws InstituteExceptions {
        setName(name);
        setCity(city);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws InstituteExceptions {
        if(name == null)
            throw new InstituteExceptions(InstituteErrorCodes.NAME_IS_NULL.getErrorCodes());
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws InstituteExceptions {
        if(city == null)
            throw new InstituteExceptions(InstituteErrorCodes.CITY_IS_NULL.getErrorCodes());
        this.city = city;
    }

    @Override
    public String toString(){
        return name + " in the " + city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Institute institute = (Institute) o;
        return Objects.equals(name, institute.name) &&
                Objects.equals(city, institute.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, city);
    }
}
