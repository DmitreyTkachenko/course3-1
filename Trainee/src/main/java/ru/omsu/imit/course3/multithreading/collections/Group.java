package ru.omsu.imit.course3.multithreading.collections;

import ru.omsu.imit.course3.collections.exceptions.GroupErrorCodes;
import ru.omsu.imit.course3.collections.exceptions.GroupExceptions;
import ru.omsu.imit.course3.Trainee.Trainee;

public class Group {
    private String name;
    private Trainee[] trainees;

    public Group(String name, Trainee[] trainees) throws GroupExceptions {
        setName(name);
        setTrainees(trainees);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws GroupExceptions {
        if(name == null)
            throw new GroupExceptions(GroupErrorCodes.NAME_IS_EMPTY.getErrorCodes());
        synchronized (name) {
            this.name = name;
        }
    }

    public Trainee[] getTrainees() {
        synchronized (trainees) {
            return trainees;
        }
    }

    public void setTrainees(Trainee[] trainees) throws GroupExceptions {
        if(trainees == null)
            throw new GroupExceptions(GroupErrorCodes.TRAINEE_IS_NULL.getErrorCodes());
        synchronized (this.trainees) {
        this.trainees = trainees;
        }
    }
}
