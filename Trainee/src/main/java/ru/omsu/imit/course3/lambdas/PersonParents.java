package ru.omsu.imit.course3.lambdas;

public class PersonParents {
        private PersonParents mother;
        private PersonParents father;

        public PersonParents(PersonParents mother, PersonParents father){
            this.mother = mother;
            this.father = father;
        }

        public PersonParents getFather() {
            return father;
        }

        public PersonParents getMother() {
            return mother;
        }

        public PersonParents getMothersMotherFather(){
            return getMother() != null ?
                    getMother().getMother() != null ?
                            getMother().getMother().getFather() : null : null;
        }

}
