package ru.omsu.imit.course3.collections.exceptions;

public class InstituteExceptions extends Exception {
    public InstituteExceptions(){
    }

    public InstituteExceptions(String errorCodes) {
        super(errorCodes);
    }
}
