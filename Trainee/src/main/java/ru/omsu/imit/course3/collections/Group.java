package ru.omsu.imit.course3.collections;

import ru.omsu.imit.course3.Trainee.Trainee;
import ru.omsu.imit.course3.collections.exceptions.GroupErrorCodes;
import ru.omsu.imit.course3.collections.exceptions.GroupExceptions;

public class Group {
    private String name;
    private Trainee[] trainees;

    public Group(String name, Trainee[] trainees) throws GroupExceptions {
        setName(name);
        setTrainees(trainees);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws GroupExceptions {
        if(name==null){
            throw new GroupExceptions(GroupErrorCodes.NAME_IS_EMPTY.getErrorCodes());
        }
        this.name = name;
    }

    public Trainee[] getTrainees() throws GroupExceptions {
        if(name==null){
            throw new GroupExceptions(GroupErrorCodes.TRAINEE_IS_NULL.getErrorCodes());
        }
        return trainees;
    }

    public void setTrainees(Trainee[] trainees) {
        this.trainees = trainees;
    }
}