package ru.omsu.imit.course3.collections.exceptions;

public enum InstituteErrorCodes {
    NAME_IS_NULL("can not be null or an empty string"),
    CITY_IS_NULL("can not be null or an empty string");
    private final String errorCodes;

    InstituteErrorCodes(String errorCodes) {
        this.errorCodes = errorCodes;
    }


    public String getErrorCodes() {
        return errorCodes;
    }
}
