package ru.omsu.imit.course3.multithreading.sixteenth.task;

public interface Executable {
    void execute();
}
