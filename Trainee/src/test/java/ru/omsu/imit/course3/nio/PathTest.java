package ru.omsu.imit.course3.nio;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PathTest {
    @Test
    public void givenPath_whenInspectsStart() {
        Path p1 = Paths.get("/file/test");
        assertTrue(p1.startsWith("/file"));
    }

    @Test
    public void givenPath_whenInspectsEnd() {
        Path p1 = Paths.get("/file/test");
        assertTrue(p1.endsWith("test"));
    }

    @Test
    public void givenTwoPaths_whenJoinsAndResolves() {
        Path p = Paths.get("/file/test");
        Path p2 = p.resolve("new");
        assertEquals("\\file\\test\\new", p2.toString());
    }

    @Test
    public void givenPath_whenConvertPaths() {
        Path relative = Paths.get("file2.txt");
        Path abs = NioTestImportantMethods.convertAbsolute(relative);
        assertEquals("C:\\Проекты студентов и преподавателей\\3 курс\\Tk\\course3-1\\Trainee\\file2.txt", abs.toString());
    }
}
