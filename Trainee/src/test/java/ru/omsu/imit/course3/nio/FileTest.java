package ru.omsu.imit.course3.nio;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import static java.awt.Event.HOME;
import static org.junit.Assert.*;

public class FileTest {
    @Test
    public void FileNotExistsTest() {
        Path p = Paths.get(HOME + "/test_file.txt");
        assertTrue(Files.notExists(p));
    }

    @Test
    public void copyTest() throws IOException{
        String res = "copyTest";
        NioTestImportantMethods.writeToFile("Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\nio\\files\\file1.txt", res);
        NioTestImportantMethods.copyFile("Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\nio\\files\\file1.txt",
                "Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\nio\\files\\file1Copy.txt" );
        byte[] strBytes = NioTestImportantMethods.readFromFile("Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\nio\\files\\file1Copy.txt");
        assertArrayEquals(strBytes, NioTestImportantMethods.readToOutputStream("Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\nio\\files\\file1Copy.txt"));

    }


    @Test
    public void delete() throws IOException {
        Files.deleteIfExists(Paths.get("files\\files_test\\toDel"));
        assertFalse(Files.exists(Paths.get("files\\files_test\\toDel")));
    }

    @Test
    public void renameTest() throws IOException {
        String dir = "files\\files_test3";
        NioTestImportantMethods.renameAllFiles(dir);

        Iterator<Path> iterator = Files.newDirectoryStream(Paths.get(dir)).iterator();
        iterator.forEachRemaining((i) ->{
            String fileName = i.getFileName().toString();
            if(fileName.substring(fileName.indexOf('.')).equals("dat"))
                assertEquals(fileName.substring(0, fileName.length() - 3) + "bin", fileName);
        });
    }

}