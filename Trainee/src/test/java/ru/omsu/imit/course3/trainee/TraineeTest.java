/*package ru.omsu.imit.course3.trainee;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.omsu.imit.course3.Trainee.*;


public class TraineeTest {
    @Test
    public void ConstructorTest() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 5);
    }

    @Test (expected = TraineeException.class)
    public void ConstructorMarkExceptionTest() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 7);
    }

    @Test (expected = TraineeException.class)
    public void ConstructorMarkExceptionTest2() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 0);
    }

    @Test (expected = TraineeException.class)
    public void ConstructorFirstNameNullExceptionTest() throws TraineeException {
        Trainee tr = new Trainee(null, "sn", 4);
    }

    @Test (expected = TraineeException.class)
    public void ConstructorFirstNameEmptyExceptionTest() throws TraineeException {
        Trainee tr = new Trainee("", "sn", 4);
    }

    @Test (expected = TraineeException.class)
    public void ConstructorFirstNameEmptyExceptionTest2() throws TraineeException {
        Trainee tr = new Trainee(" ", "sn", 4);
    }

    @Test (expected = TraineeException.class)
    public void ConstructorSecondNameNullExceptionTest() throws TraineeException {
        Trainee tr = new Trainee("fn", null, 4);
    }

    @Test (expected = TraineeException.class)
    public void ConstructorSecondNameEmptyExceptionTest() throws TraineeException {
        Trainee tr = new Trainee("fn", "", 4);
    }

    @Test (expected = TraineeException.class)
    public void ConstructorSecondNameEmptyExceptionTest2() throws TraineeException {
        Trainee tr = new Trainee("fn", " ", 4);
    }

    @Test (expected = TraineeException.class)
    public void SetterSetFirstNameNullException() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);
        tr.setName(null);
    }

    @Test (expected = TraineeException.class)
    public void SetterSetFirstNameEmptyException() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);
        tr.setName("");
    }

    @Test (expected = TraineeException.class)
    public void SetterSetFirstNameEmptyException2() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);
        tr.setName(" ");
    }

    @Test (expected = TraineeException.class)
    public void SetterSetSecondNameNullException() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);
        tr.setSurname(null);
    }

    @Test (expected = TraineeException.class)
    public void SetterSetSecondNameEmptyException() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);
        tr.setSurname("");
    }

    @Test (expected = TraineeException.class)
    public void SetterSetSecondNameEmptyException2() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);
        tr.setSurname(" ");
    }

    @Test (expected = TraineeException.class)
    public void SetterSetMarkException() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);
        tr.setScore(0);
    }

    @Test (expected = TraineeException.class)
    public void SetterSetMarkException2() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);
        tr.setScore(6);
    }

    @Test
    public void SetterTest() throws TraineeException {
        Trainee tr = new Trainee("fn", "sn", 4);

        tr.setName("fn1");
        assertEquals("fn1", tr.getName());

        tr.setSurname("sn2");
        assertEquals("sn2", tr.getSurname());

        tr.setScore(2);
        assertEquals(2, tr.getScore());
    }

}

*/