package ru.omsu.imit.course3.trainee;

import org.junit.Test;
import ru.omsu.imit.course3.Trainee.Rectangle;
import ru.omsu.imit.course3.Trainee.TestMethods;
import ru.omsu.imit.course3.Trainee.Trainee;
import ru.omsu.imit.course3.Trainee.TraineeException;

import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class WriteReadTest {
    @Test
    public void rectangleWriteTest() throws IOException {
        Rectangle rectangle = new Rectangle(2, 4, 6, 8);
        TestMethods.rectangleWrite("",rectangle);
        Rectangle rectangle1 = TestMethods.rectangleRead(rectangle,"");

        assertEquals(2.0, rectangle1.getLeftTop().x, 0.0);
        assertEquals(4.0, rectangle1.getLeftTop().y, 0.0);
        assertEquals(6.0, rectangle1.getRightBottom().x, 0.0);
        assertEquals(8.0, rectangle1.getRightBottom().y, 0.0);
    }

    @Test
    public void traineeWriteTest() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Дмитрий", "Ткаченко", 4);
        TestMethods.traineeWriteWithLineSeparation(trainee,"");
        Trainee trainee1 = TestMethods.traineeRead("");
        assertEquals(trainee, trainee1);
    }

    @Test
    public void traineeWriteTest2() throws TraineeException, IOException {
        Trainee trainee = new Trainee("Дмитрий", "Ткаченко", 4);
        TestMethods.traineeWrite(trainee,"");
        Trainee trainee1 = TestMethods.traineeRead2("");
        assertEquals(trainee, trainee1);
    }

    @Test
    public void serializeTest() throws TraineeException, IOException {
                Trainee trainee = new Trainee("Дмитрий", "Ткаченко", 4);
                TestMethods.serializeWrite("",TestMethods.serialize(trainee));
                Trainee trainee1 = TestMethods.serializeRead("");
                        
               assertEquals(trainee, trainee1);
           }

}