package ru.omsu.imit.course3.trainee;

import org.junit.Test;
import ru.omsu.imit.course3.Trainee.Rectangle;


import java.io.*;

import static org.junit.Assert.assertEquals;


public class RectangleTest {
    @Test
    public void RectangleTest() throws IOException {
        Rectangle rect = new Rectangle(-1.0,14.0,20.0,0.0);
        DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("C:\\Users\\Student\\Downloads\\course3-1\\Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\Trainee\\RectangleTest\\rectangle.bin"));
        outputStream.writeDouble(rect.getLeftTop().x);
        outputStream.writeDouble(rect.getLeftTop().y);
        outputStream.writeDouble(rect.getRightBottom().x);
        outputStream.writeDouble(rect.getRightBottom().y);

        DataInputStream inputStream = new DataInputStream(new FileInputStream("C:\\Users\\Student\\Downloads\\course3-1\\Trainee\\src\\main\\java\\ru\\omsu\\imit\\course3\\Trainee\\RectangleTest\\rectangle.bin"));

        double x1 = inputStream.readDouble();
        double x2 = inputStream.readDouble();
        double x3 = inputStream.readDouble();//
        double x4 = inputStream.readDouble();

            Rectangle rectangle1 = new Rectangle(x1,  x2, x3, x4);
            assertEquals(-1.0, rectangle1.getLeftTop().x, 0.0);
            assertEquals(14.0, rectangle1.getLeftTop().y, 0.0);
            assertEquals(20.0, rectangle1.getRightBottom().x, 0.0);
            assertEquals(0.0, rectangle1.getRightBottom().y, 0.0);

    }
}
